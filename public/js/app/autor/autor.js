jQuery(document).ready(function(){

    if($("#id").val() != 0)
    {
        jQuery.ajax({
            url: '../../api/autores/get/'+$("#id").val(),
            success: function(data) {
    
                $("#fotoActual").hide(); 
                
                jQuery.each(data.data, function(index, value) {
                    console.log("index - ",index);
                    console.log("value - ",value);
                    $("#"+index).val(value);
    
                    if(index == "foto"){
                        var src = "/imagenes/autores/"+value;
                        jQuery('#autorFoto').attr('src', src);
                        $("#fotoActual").show(); 
                    }
                });
            }
        });
    }
 
    jQuery('#guardarautor').submit(function(event) {
        event.preventDefault();
        var ext = $("#archivo2").val().substring($("#archivo2").val().lastIndexOf('.') + 1);
        var formData = new FormData($(this)[0]);
        formData.append("ext", ext);

        jQuery.ajax({
            url: '../../api/autores/store',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            cache: false,
            contentType: false,
            processData: false,
            data: formData, //jQuery('#guardarproveedor').serialize(),
            success: function(data) {
                var src = "/imagenes/autores/"+data.logo;
                alert(data.message);
                jQuery('#id').val(data.id);
                jQuery('#autorFoto').attr('src', src);
            },
            error: function(data) {
                console.log(data.message);
            }
        });
    });
 });