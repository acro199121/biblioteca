
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header"><a href="{{url("/autores")}}">Listado de Autores</a> - Crear Autor </div>
    
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="col-lg-8 offset-lg-2 rounded shadow">
                        <form enctype="multipart/form-data" id="guardarautor" method="post" >
                            
                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                        
                            <p><b>Los campos obligatorios se identificaran con un asterisco (*)</b></p>
                
                            <div class="form-group">
                                <label>Primer nombre *</label>
                                    <input type="text" class="form-control" id="primer_nombre" name="primer_nombre" placeholder="Digitar el primer nombre del autor" required="required">
                            </div>

                            <div class="form-group">
                                <label>Segundo nombre</label>
                                <input type="text" class="form-control" id="segundo_nombre" name="segundo_nombre" placeholder="Digitar el segundo nombre del autor si este lo tiene">
                            </div>
                            
                            <div class="form-group">
                                <label>Primer apellido *</label>
                                <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Digitar el primer apellido del autor" required="required">
                            </div>

                            <div class="form-group">
                                <label>Segundo apellido</label>
                                <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Digitar el segundo apellido del autor si este lo tiene">
                            </div>
                            
                            <div class="form-group">
                                <label>Fecha de nacimiento *</label>
                                <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento">
                            </div>

                            <div class="form-group">
                                <label>Breve reseña del autor</label>
                                <textarea class="form-control" rows="5" name="resena_autor" id="resena_autor">
                                </textarea>
                            </div>
                            
                            <div class="form-group" id="log">
                                <label>Foto</label><br>
                                <div class="mb-3" id="fotoActual">
                                    <img src="" id="autorFoto" class="img-fluid img-thumbnail mx-auto d-block mt-1">
                                </div>
                                <input  type="file" id="archivo2" name="archivo2"/>
                            </div>
                
                            <button type="submit" class="btn btn-primary mb-3">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/app/autor/autor.js')}}"></script>
@endsection