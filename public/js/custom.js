function showToast(text) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    x.innerHTML = text;
  
    // Add the "show" class to DIV
    x.className = "show";
  
    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }

  function opcionesFormatter(value, row, index, field) {
      return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a>';
  }

  function opcionesProveedorFormatter(value, row, index, field) {
      return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a> / <a href="'+window.location.href+'/dashboardproveedor/'+row.id+'">Ver perfil del proveedor</a> / <a href="'+window.location.href+'/calificar/'+row.id+'">Calificar proveedor</a>';
  }

  function deleteFormatter(value, row, index, field) {
    return '<a href="#" onClick="return deleteItemsPlantilla('+row.id+');" data-id="'+row.id+'">Eliminar</a>';
  }

  function fechaFormartter (value, row, index, field){
      return ''+row.ano+'/'+row.mes+'';
  }

  function estadoFormartter (value, row, index, field){

      let estado = "";

      if(row.estado == 0)
         estado = "Espera";
      if (row.estado == 1)
          estado = "Aprobada";
      if (row.estado == 2)
          estado = "Rechazada";

      return estado;
  }

  function estadoPaFormartter (value, row, index, field){

      let estado = "";

      if(row.estadoplandeaccion == 0)
         estado = "Espera";
      if (row.estadoplandeaccion == 1)
          estado = "Aprobada";
      if (row.estadoplandeaccion == 2)
          estado = "Rechazada";

      return estado;
  }

  function opcionesCalificacionFormatter(value, row, index, field){

      let editar = '<a href="calificaciones/calificar/'+row.proveedore_id+'/1">Editar</a>';
      let aprobar = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',1)">Aprobar</a>';
      let rechazar = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',2)">Rechazar</a>';
      let planAccion = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',3)">Aprobar plan de accion</a>'

      let opciones = "";

      if(row.estado == 0)
          opciones = ''+aprobar+' / '+rechazar+'';

      if(row.estado == 2)
          opciones = ''+aprobar+' / '+editar+''
      
      if(row.plandeaccion != "" && row.estadoplandeaccion == 0 && row.estado != 1)
          opciones += ' / '+planAccion+'';

      return opciones;
  }
  
    
function deleteItem(id) {

    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: 'api/'+basecrud+'/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                console.log(data);
                jQuery('#table').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }

    return false;
}


function deleteItemsPlantilla(id) {
    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: 'api/itemsPlantillas/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                jQuery('#table_item').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }
    return false;
}


function cambiarEstadoCalificacion(id,estado){
  jQuery.ajax({
      url: 'api/calificaciones/cambiarEstados/'+id+'/'+estado,
      type: 'GET',
      success:function(data) {
        showToast(data.message);
        jQuery('#table').bootstrapTable('refresh');
      },
      error: function(data) {
        showToast(data.message);
      }
  });

  return false;
}
  
  