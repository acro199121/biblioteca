<?php

namespace App\Http\Controllers;

use App\Models\articulos;
use Illuminate\Http\Request;
use App\Http\Controllers\services\articulos\articulosServices;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new articulosServices();
        $rest = $service->crearArticulo($request);
        return response()->json($rest);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\articulos  $articulos
     * @return \Illuminate\Http\Response
     */
    public function show(articulos $articulos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\articulos  $articulos
     * @return \Illuminate\Http\Response
     */
    public function edit(articulos $articulos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\articulos  $articulos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, articulos $articulos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\articulos  $articulos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $service = new articulosServices();
        $rest = $service->eliminarArticulo($request->id);

        return response()->json($rest);
    }

      /**
     * Metodo para listar un articulo por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $service = new articulosServices();
        $rest = $service->getArticulo($request);
        return response()->json($rest);
    }

    /**
     * Metodo para cosultar articulos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request){
        $service = new articulosServices();
        $rest = $service->listarArticulos($request);
        return response()->json($rest);
    }

    public function getAutoresArt(Request $request)
    {   
        $service = new articulosServices();
        $rest = $service->selectAutoresArticulo($request->id);
        return response()->json($rest);
    }
}
