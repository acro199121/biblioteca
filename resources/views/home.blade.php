@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Menu Principal</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-6 col-md-4">
                            <div class="contenMenu">
                                <a href="{{ url('autores') }}">
                                    <strong> Autores </strong>

                                    <div class="mt-3 iconsize">
                                        <span class="fas fa-user-edit"></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="contenMenu">
                                <a href="{{ url('libros') }}">
                                    <strong> Libros </strong>

                                    <div class="mt-3 iconsize">
                                        <span class="fas fa-book"></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-md-4">
                            <div class="contenMenu">
                                <a href="{{ url('articulos') }}">
                                    <strong> Articulos </strong>

                                    <div class="mt-3 iconsize">
                                        <span class="fas fa-file-alt"></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
