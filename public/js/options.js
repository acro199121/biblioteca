
function opcionesFormatter(value, row, index, field) {
    return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> - <a href="'+window.location.href+'/form/'+row.id+'">Editar</a>';
}

function fotoFotmatter(value, row, index, field){
    console.log("estos son los datos", row);
    return '<img id="fotoAutor" class="sizeImg" src="/imagenes/autores/'+row.foto+'" />';
}


function deleteItem(id) {

    if (confirm('¿Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: 'api/'+basecrud+'/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                alert(data.message);
                jQuery('#table').bootstrapTable('refresh');
            },
            error: function(data) {
                console.log(data.message);
            }
        });
    }

    return false;
}