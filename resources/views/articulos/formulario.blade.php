@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header"><a href="{{url("/articulos")}}">Listado de Articulos</a> - Crear Articulo </div>
    
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="col-lg-8 offset-lg-2 rounded shadow">
                        <form id="guardarArt" method="post" >
                            
                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                        
                            <p><b>Los campos obligatorios se identificaran con un asterisco (*)</b></p>
                
                            <div class="form-group">
                                <label>nombre *</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Digite el nombre del articulo" required="required">
                            </div>

                            <div class="form-group">
                                <label>Tipo *</label>
                                <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Digitar el tipo Ejem: Cientifico" required="required">
                            </div>

                            <div class="form-group">
                                <label>Autor(s)</label>
                                <select class="autores form-control" id="autores" name="autores[]" multiple="multiple" required="required">
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Breve reseña del articulo</label>
                                <textarea class="form-control" rows="5" name="resena" id="resena" required="required">
                                </textarea>
                            </div>

                            <button type="submit" class="btn btn-primary mb-3">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/app/articulo/articulo.js')}}"></script>
@endsection