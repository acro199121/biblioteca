<?php

namespace App\Http\Controllers;

use App\Models\autor;
use Illuminate\Http\Request;
use App\Http\Controllers\services\autores\autoresServices;

class AutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
        $service = new autoresServices();
        $rest = $service->crearAutor($request);

        return $rest ;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function show(autor $autor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function edit(autor $autor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\autor  $autor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, autor $autor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $service = new autoresServices();
        $rest = $service->eliminarAutor($request->id);

        return response()->json($rest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $service = new autoresServices();
        $rest = $service->listarAutor($request->id);
        return response()->json($rest);   
    }

    /**
     * Metodo para consultar autores
     *
     * @param  Request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $service = new autoresServices();

        $rest = $service->listarAutores($request);

        return response()->json($rest);

    }
}
