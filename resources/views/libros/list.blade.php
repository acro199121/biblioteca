@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Listado de libros</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="col-md-12 mt-4" style="padding-left:0px;">
                        <a href="{{ url ("libros/form") }}" class="btn btn-primary" role="button">Crear libro</a>
                    </div>
                    
                    <table id="table" data-pagination="true" data-side-pagination="server" data-search="true" data-toggle="table" data-url="<?php echo url('api/libros/list'); ?>">
                        <thead>
                            <tr>
                                <th data-field="nombre">Nombre</th>
                                <th data-field="fecha_publicacion">Publicación</th>
                                <th data-field="num_paginas">paginas</th>
                                <th data-field="num_copias">copias</th>
                                <th data-field="autor">autor</th>
                                <th data-field="resena">Reseña</th>
                                <th data-formatter="opcionesFormatter">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var basecrud = 'libros';
    </script>
@endsection
