@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Listado de Autores</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="col-md-12 mt-4" style="padding-left:0px;">
                        <a href="{{ url ("autores/form") }}"><span class="fas fa-user-plus iconOpSize" title="Crear autor"></span></a>
                    </div>
                    
                    <table id="table" data-pagination="true" data-side-pagination="server" data-search="true" data-toggle="table" data-url="<?php echo url('api/autores/list'); ?>">
                        <thead>
                            <tr>
                                <th data-formatter="fotoFotmatter">Foto</th>
                                <th data-field="nombre">Nombre</th>
                                <th data-field="fecha_nacimiento">Nacimiento</th>
                                <th data-field="resena_autor">Reseña</th>
                                <th data-formatter="opcionesFormatter">Opciones</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var basecrud = 'autores';
    </script>
@endsection
