<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::view('/registro', 'auth/register');
Route::view('/autores', 'autores/list');
Route::view('/libros', 'libros/list');
Route::view('/articulos', 'articulos/list');


Route::get('/autores/form/{id?}', function($id = 0){
    return view('autores/formulario', ['id' => $id]);
});

Route::get('/libros/form/{id?}', function($id = 0){
    return view('libros/formulario', ['id' => $id]);
});

Route::get('/articulos/form/{id?}', function($id = 0){
    return view('articulos/formulario', ['id' => $id]);
});