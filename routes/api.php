<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('soap/show', 'SoapController@show');
Route::post('profile/getAllProfile', 'ProfileController@getAllProfile');
Route::post('registro/store', 'RegistroUserController@store');

Route::get('autores/get/{id}', 'AutorController@get');
Route::get('autores/list', 'AutorController@list');
Route::post('autores/store', 'AutorController@store');
Route::delete('autores/destroy/{id}', 'AutorController@destroy');

Route::get('libros/get/{id}', 'LibroController@get');
Route::get('libros/list', 'LibroController@list');
Route::post('libros/store', 'LibroController@store');
Route::delete('libros/destroy/{id}', 'LibroController@destroy');
Route::post('libros/selectLibros', 'LibroController@selectLibros');

Route::get('articulos/get/{id}', 'ArticuloController@get');
Route::get('articulos/list', 'ArticuloController@list');
Route::post('articulos/store', 'ArticuloController@store');
Route::delete('articulos/destroy/{id}', 'ArticuloController@destroy');
Route::get('articulos/getAutoresArt/{id}', 'ArticuloController@getAutoresArt');
