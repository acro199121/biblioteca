<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkProfileInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('id_profile')->nullable();
            $table->foreign('id_profile')->references('id')->on('profile');
        });

        \DB::table('users')->insert(
            array(
                'name' => 'administrador',
                'email' => 'administrador@admin.co',
                'password' => '$2y$10$zETY.6PMcJJZ22QZom0ZMOoCVu1fW7qrlrwXzggE2OvB8ERWjzK6m',
                'remember_token' => 'mJrLzDSYj7eVM2kFh3kXrFjFfj5j757GpzFbeJIvZhQngMbHbAOvp8spynEI',
                'id_profile' => 1,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
