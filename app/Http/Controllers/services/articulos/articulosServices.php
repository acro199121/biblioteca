<?php

    namespace App\Http\Controllers\services\articulos;

    use App\Models\articulos;
    use App\Models\ArticulosAutores;
    use App\Models\autor;

class articulosServices 
    {
        public function listarArticulos ($datos)
        {
            $message = "";
            $data = [];
            
            $query = articulos::where("estado",0)->select("articulos.*");

            if($datos->search !== null)
            {
                $query = articulos::where('nombre', 'like', '%'.$datos->search.'%')
                ->OrWhere("estado",0)
                ->select("articulos.*");

                if($datos->offset !== null && $datos->limit !== null){
                    $query->skip($datos->offset)->take($datos->limit);
                }
    
                $query = $query->get();
            }
            else if(($datos->offset !== null) && ($datos->limit !== null)){

                $query = $query->skip($datos->offset)->take($datos->limit)->get();
            }
    
            $total = $query->count();


            if($query !== null){
                $message = "Consulta exitosa";
                $data = $query;
            }else{
                $message = "No se encontro Información para los datos enviados";
            }

            $response = array(
                'status' => 201,
                'message' => $message,
                'rows' => $data,
                'total' => $total 
            );
    
            return $response;
        }

        public function crearArticulo($datos)
        {
            if($datos->id == 0){

                $art = new articulos();
                $art->nombre = $datos->nombre;
                $art->resena = $datos->resena;
                $art->tipo = $datos->tipo;
                

            }else{
                $art = articulos::find($datos->id);
                
                if($art !== null)
                {    
                    $art->nombre = $datos->nombre;
                    $art->resena = $datos->resena;
                    $art->tipo = $datos->tipo;
                }
            }

            $art->save();

            $objArt = articulos::find($art->id);

            if($objArt !== null){
                $status = 201;
                $message = "Datos guardados con exito";
                ## se asocia el articulo con los autores
                $this->asociarAutorArticulo($datos->autores,$art->id, $datos->fecha_publicacion);
            }else{
                $status = 201;
                $message = "Error: No se pudo guardar la información";
            }

            $response = array(
                'id' => $art->id,
                'status' => $status,
                'message' => $message
            );

            return $response;
        }

        public function eliminarArticulo($id){
            
            $message = "Articulo eliminado";

            $query = articulos::find($id);
            $query->estado=1;
            $query->save();

            $response = array(
                'status' => 201,
                'message' => $message
            );


            return $response;
        }

        public function getArticulo ($request)
        {
            $query = articulos::find($request->id);

            if($query !== null){
                $data = $query;
                $status = 201;
                $mensaje = "Consulta exitosa";
            }
            else{
                $data = [];
                $status = 402;
                $mensaje = "No se pudo realizar la consulta";
            }

            $response = [
                'status' => $status,
                'message' => $mensaje,
                'data' =>  $data
            ];

            return $response;
        }

        public function asociarAutorArticulo ($autores, $id_articulo,$fecha_publicacion)
        {
            ArticulosAutores::where("id_articulo", $id_articulo)->delete();

            foreach ($autores as $value) {
                $query = new ArticulosAutores();
                $query->id_autor = $value;
                $query->id_articulo = $id_articulo;
                //$query->fecha_publicacion = "2019-04-24";
                $query->save();
            }
        }

        public function selectAutoresArticulo($id)
        {   
            $select = ArticulosAutores::join("autor", "articulos_autores.id_autor", "=", "autor.id")
            ->select(\DB::raw("CONCAT(autor.primer_nombre,' ',autor.primer_apellido) as nombre, autor.id AS id_aut"))
            ->where("id_articulo",$id)
            ->get();
                
            return $select;
        }
    }

?>