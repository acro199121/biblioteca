<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_autor
 * @property string $nombre
 * @property string $resena
 * @property string $fecha_publicacion
 * @property string $num_paginas
 * @property string $num_copias
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Autor $autor
 */
class libros extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_autor', 'nombre', 'resena', 'fecha_publicacion', 'num_paginas', 'num_copias', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autor()
    {
        return $this->belongsTo('App\Autor', 'id_autor');
    }
}
