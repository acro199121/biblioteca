<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("primer_nombre",255);
            $table->string("segundo_nombre",255)->nullable();
            $table->string("primer_apellido",255);
            $table->string("segundo_apellido",255)->nullable();
            $table->date("fecha_nacimiento");
            $table->text("resena_autor")->nullable();
            $table->string("foto")->nullable();
            $table->timestamps();
            $table->softDeletes();	
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autor');
    }
}
