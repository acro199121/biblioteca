<?php

    namespace App\Http\Controllers\services\autores;

    use App\Models\autor;

    class autoresServices {

        public function listarAutor($id)
        {
            $query = autor::find($id);

            if($query !== null){
                $data = $query;
                $status = 201;
                $mensaje = "Consulta exitosa";
            }
            else{
                $data = [];
                $status = 402;
                $mensaje = "No se pudo realizar la consulta";
            }

            $response = [
                'status' => $status,
                'message' => $mensaje,
                'data' =>  $data
            ];

            return $response;
        }


        public function listarAutores ($datos)
        {
            $message = "";
            $data = [];
            
            $autor = autor::select("autor.*",\DB::raw("CONCAT(primer_nombre,' ',primer_apellido) as nombre"));

            if($datos->search !== null)
            {
                $autor = autor::where('primer_nombre', 'like', '%'.$datos->search.'%')
                ->OrWhere('segundo_nombre', 'like', '%'.$datos->search.'%')
                ->OrWhere('primer_apellido', 'like', '%'.$datos->search.'%')
                ->OrWhere('segundo_apellido', 'like', '%'.$datos->search.'%')
                ->select("autor.*",\DB::raw("CONCAT(primer_nombre,' ',primer_apellido) as nombre"));

                if($datos->offset !== null && $datos->limit !== null){
                    $autor->skip($datos->offset)->take($datos->limit);
                }
    
                $autor = $autor->get();
            }
            else if(($datos->offset !== null) && ($datos->limit !== null)){

                $autor = $autor->skip($datos->offset)->take($datos->limit)->get();
            }
    
            $total = $autor->count();


            if($autor !== null){
                $message = "Consulta exitosa";
                $data = $autor;
            }else{
                $message = "No se encontro Información para los datos enviados";
            }

            $response = array(
                'status' => 201,
                'message' => $message,
                'rows' => $data,
                'total' => $total 
            );
    
            return $response;
        }

        public function crearAutor($datos)
        {
            $rutaTemp = $datos->archivo2;
            $nombreFoto = "";
            $resP = false;

            if($datos->id == 0){

                $autor = new autor();
                $autor->primer_nombre = $datos->primer_nombre;
                $autor->segundo_nombre = $datos->segundo_nombre;
                $autor->primer_apellido = $datos->primer_apellido;
                $autor->segundo_apellido = $datos->segundo_apellido;
                $autor->fecha_nacimiento = $datos->fecha_nacimiento;
                $autor->resena_autor = $datos->resena_autor;
            }else{
                $autor = autor::find($datos->id);
                if($autor !== null)
                {    
                    $autor->primer_nombre = $datos->primer_nombre;
                    $autor->segundo_nombre = $datos->segundo_nombre;
                    $autor->primer_apellido = $datos->primer_apellido;
                    $autor->segundo_apellido = $datos->segundo_apellido;
                    $autor->fecha_nacimiento = $datos->fecha_nacimiento;
                    $autor->resena_autor = $datos->resena_autor;
                }
            }

            $autor->save();

            $objAutor = autor::find($autor->id);

            if($objAutor !== null){
                $status = 201;
                $message = "Datos guardados con exito";
                
                $nombreFoto = "".$autor->id.".".$datos->ext."";
                if($rutaTemp != null)
                    $resP = $this->uploadLogo($nombreFoto, $rutaTemp);
                
                if($resP)
                    $objAutor->foto = $nombreFoto;
                    $objAutor->save();
            }else{
                $status = 201;
                $message = "Error: No se pudo guardar la información";
            }

            $response = array(
                'id' => $autor->id,
                'logo' => $objAutor->foto,
                'status' => $status,
                'message' => $message
            );

            return $response;
        }

        public function eliminarAutor($id){
            
            autor::where('id',$id)->delete();
        
            $rest = autor::find($id);

            if($rest === null){
                $message = "autor eliminado";
            }else{
                $message = "No se pudo eliminar el autor";
            }

            $response = array(
                'status' => 201,
                'message' => $message
            );


            return $response;
        }

     
        public function uploadLogo($archivo,$ruta)
        {
            $dir_subida = '../public/imagenes/autores/';
            $fichero_subido = $dir_subida . basename($archivo);
            move_uploaded_file($ruta, $fichero_subido);
            chmod($fichero_subido, 0777);
            
            return response()->json(true);
        }

        public function selectAutores($request)
        {
            $search = $request->serach;

            if($search != "" ){
                $query = autor::where('primer_nombre', 'like', '%'.$search.'%')
                ->OrWhere('segundo_nombre', 'like', '%'.$search.'%')
                ->OrWhere('primer_apellido', 'like', '%'.$search.'%')
                ->OrWhere('segundo_apellido', 'like', '%'.$search.'%')
                ->select("autor.*",\DB::raw("CONCAT(primer_nombre,'',primer_apellido) as nombre"))
                ->get();
            }else{
                $query = autor::select("autor.*",\DB::raw("CONCAT(primer_nombre,' ',primer_apellido) as nombre"))->get();
            }

            return $query;
        }

    }

?>