@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="alert alert-primary col-md-8 " style="display:none" role="alert" id="alertError">
            <button type="button" class="close cerrarAlert" data-alerta="alertError">
                <span aria-hidden="true">&times;</span>
            </button>
            <h2 class="alert-heading">Alerta!</h2>
            <strong><h4 id="error"><h4></strong>
        </div>

        <div class="alert alert-success col-md-8" style="display:none" role="alert" id="alertExito">
            <button type="button"  class="close cerrarAlert" data-alerta="alertExito">
                <span aria-hidden="true">&times;</span>
            </button>
            <h2 class="alert-heading">Exito!</h4>
            <strong><h4 id="mensaje"><h4></strong>
            <ul>
                <li><h2 id="email"><h2></li>
                <li><h2 id="pass"><h2> </li>
            </ul>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" id="guardarusuario">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Profile') }}</label>

                            <div class="col-md-6">
                                <select id="profile" class="form-control {{ $errors->has('id_profile') ? ' is-invalid' : '' }}"  name="profile" required="required">
                                    <option value="">Select profile</option>
                                </select>

                                @if ($errors->has('id_profile'))
                                     <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_profile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            
                                <span class="invalid-feedback error_passw" role="alert" style="display:none;">
                                    <strong id="error_pass"></strong>
                                </span>
                            
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary regis" disabled>
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        jQuery(document).ready(function(){

            $(".cerrarAlert").click(function(){
                let divAlert = "#"+$(this).data('alerta')+"";
                $(divAlert).fadeOut();
            })

            $("#password-confirm").keyup(function(){

                let contraseña = $("#password").val();
                let confirm = $("#password-confirm").val();
                let error = "Las contraseñas no coinciden";

                if(confirm != contraseña){
                    $("#error_pass").html(error);
                    $(".error_passw").css("display",'initial');
                }
                else{
                    $(".error_passw").css("display",'none');
                    $(".regis").removeAttr("disabled");
                }
            })

            jQuery.ajax({
                url: '../../api/profile/getAllProfile',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {}, 
                success: function(data) {

                    var html = "";

                    if(data.state == 201){
                        for(item in data.data){
                            html += "<option value='"+data.data[item].id+"'>"+data.data[item].nombre+"</option>"
                        }
                    }

                    $("#profile").append(html);
                },
                error: function(data) {
                }
            });

            jQuery('#guardarusuario').submit(function(event) {
                event.preventDefault();
                jQuery.ajax({
                    url: 'api/registro/store',
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: jQuery('#guardarusuario').serialize(),
                    success: function(data) {

                        if(data.status == 401)
                        {
                            $("#error").html(data.mensaje);
                            $("#alertError").fadeIn();
                            $("#alertExito").fadeOut();
                        }

                        if(data.status == 201)
                        {
                            $("#mensaje").html(data.mensaje);
                            $("#email").html("Email: "+data.correo);
                            $("#pass").html("Contraseña: "+data.password);
                            $("#alertError").fadeOut();
                            $("#alertExito").fadeIn();
                        }

                    console.log(data);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection
