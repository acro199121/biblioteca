<?php

    namespace App\Http\Controllers\services\libros;

    use App\Models\libros;

    class librosServices 
    {


        public function listarLibros($request)
        {
            $message = "";
            $data = [];
            
            $query = libros::join("autor","libros.id_autor", "=", "autor.id")
            ->select("libros.*",\DB::raw("CONCAT(autor.primer_nombre,' ',autor.primer_apellido) as autor"));

            if($request->search !== null){

                $query = libros::join("autor","libros.id_autor", "=", "autor.id")
                ->where("nombre","like","%".$request->search."%")
                ->OrWhere("autor.primer_nombre","like","%".$request->search."%")
                ->OrWhere("autor.segundo_nombre","like","%".$request->search."%")
                ->OrWhere("autor.primer_apellido","like","%".$request->search."%")
                ->OrWhere("autor.segundo_apellido","like","%".$request->search."%")
                ->select("libros.*",\DB::raw("CONCAT(autor.primer_nombre,' ',autor.primer_apellido) as autor"));

                if($request->offset !== null && $request->limit !== null){
                    $query->skip($request->offset)->take($request->limit);
                }
    
                $query = $query->get();
            }
            else if(($request->offset !== null) && ($request->limit !== null)){

                $query = $query->skip($request->offset)->take($request->limit)->get();
            }

            $total = $query->count();

            if($query !== null){
                $message = "Consulta exitosa";
                $data = $query;
            }else{
                $message = "No se encontro Información para los datos enviados";
            }

            $response = array(
                'status' => 201,
                'message' => $message,
                'rows' => $data,
                'total' => $total 
            );
    
            return $response;
        }



        public function crearLibro($datos)
        {
            if($datos->id == 0){

                $libro = new libros();
                $libro->nombre = $datos->nombre;
                $libro->resena = $datos->resena;
                $libro->fecha_publicacion = $datos->fecha_publicacion;
                $libro->num_paginas = $datos->num_paginas;
                $libro->num_copias = $datos->num_copias;
                $libro->id_autor = $datos->id_autor;
            }else{
                $libro = libros::find($datos->id);
                
                if($libro !== null)
                {    
                    $libro->nombre = $datos->nombre;
                    $libro->resena = $datos->resena;
                    $libro->fecha_publicacion = $datos->fecha_publicacion;
                    $libro->num_paginas = $datos->num_paginas;
                    $libro->num_copias = $datos->num_copias;
                    $libro->id_autor = $datos->id_autor;
                }
            }

            $libro->save();

            $objlibro = libros::find($libro->id);

            if($objlibro !== null){
                $status = 201;
                $message = "Datos guardados con exito";
            }else{
                $status = 201;
                $message = "Error: No se pudo guardar la información";
            }

            $response = array(
                'id' => $libro->id,
                'status' => $status,
                'message' => $message
            );

            return $response;
        }

        public function getLibro ($request)
        {
            $query = libros::find($request->id);

            if($query !== null){
                $data = $query;
                $status = 201;
                $mensaje = "Consulta exitosa";
            }
            else{
                $data = [];
                $status = 402;
                $mensaje = "No se pudo realizar la consulta";
            }

            $response = [
                'status' => $status,
                'message' => $mensaje,
                'data' =>  $data
            ];

            return $response;
        }

        public function eliminarLibro($id){
            
            libros::where('id',$id)->delete();
        
            $rest = libros::find($id);

            if($rest === null){
                $message = "libro eliminado";
            }else{
                $message = "No se pudo eliminar el libro";
            }

            $response = array(
                'status' => 201,
                'message' => $message
            );


            return $response;
        }

    }
?>