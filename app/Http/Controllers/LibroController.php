<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\services\libros\librosServices;
use App\Http\Controllers\services\autores\autoresServices;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new librosServices();
        $rest = $service->crearLibro($request);
        return response()->json($rest);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function show(libros $libros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function edit(libros $libros)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, libros $libros)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\libros  $libros
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $service = new librosServices();
        $rest = $service->eliminarLibro($request->id);

        return response()->json($rest);
    }

    /**
     * Metodo para listar un libro por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $service = new librosServices();
        $rest = $service->getLibro($request);
        return response()->json($rest);
    }

     /**
     * Metodo para cosultar los libros
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request){
        $service = new librosServices();
        $rest = $service->listarLibros($request);
        return response()->json($rest);
    }

    /**
     * Metodo que carga la informacion de los libros para un select
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selectLibros (Request $request)
    {
        $service = new autoresServices();
        $rest = $service->selectAutores($request);
        return response()->json($rest);
    }
}
