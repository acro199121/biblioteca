jQuery(document).ready(function(){

    $('.autores').select2({
        placeholder: "Seleccione Autor(s)",
        ajax: {
            url: '../../api/libros/selectLibros',
            method: 'POST',
            data: function(params){
                return{
                   serach:params.term 
                };
            },
            processResults: function (data) {
                return {
                    results:$.map(data, function(val,i){
                        return{id:val.id, text:val.nombre};
                    })
                };
            }
        }
    });   

 
    if($("#id").val() != 0)
    { 
        jQuery.ajax({
            url: '../../api/articulos/get/'+$("#id").val(),
            success: function(data) {

                jQuery.each(data.data, function(index, value) {
                    console.log("index - ",index);
                    console.log("value - ",value);
                    $("#"+index).val(value);
                });
            }
        });

        jQuery.ajax({
            url: '../../api/articulos/getAutoresArt/'+$("#id").val(),
            success: function(datos) {
                datos.forEach(element => {
                    $(".autores").data('select2').trigger('select', {
                        data: {"id":element.id_aut, "text": element.nombre}
                    });   
                });
            }
        });

    }

    jQuery('#guardarArt').submit(function(event) {
        event.preventDefault();

        jQuery.ajax({
            url: '../../api/articulos/store',
            type: 'POST',
            data: jQuery('#guardarArt').serialize(),
            success: function(data) {
                alert(data.message);
                jQuery('#id').val(data.id);
            },
            error: function(data) {
                console.log(data.message);
            }
        });
    });
 });