<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $fecha_nacimiento
 * @property string $resena_autor
 * @property string $foto
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property ArticulosAutore[] $articulosAutores
 * @property Libro[] $libros
 */
class autor extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'autor';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'resena_autor', 'foto', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articulosAutores()
    {
        return $this->hasMany('App\ArticulosAutore', 'id_autor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function libros()
    {
        return $this->hasMany('App\Libro', 'id_autor');
    }
}
