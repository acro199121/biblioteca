<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticulosAutores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos_autores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("id_autor");
            $table->unsignedBigInteger("id_articulo");
            /*$table->date("fecha_publicacion");*/
            $table->foreign("id_autor")->references("id")->on("autor");
            $table->foreign('id_articulo')->references('id')->on('articulos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos_autores');
    }
}
