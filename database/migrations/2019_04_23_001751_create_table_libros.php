<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLibros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nombre",255);
            $table->text("resena");
            $table->date("fecha_publicacion");
            $table->string("num_paginas",255);
            $table->string("num_copias",255);
            $table->unsignedBigInteger("id_autor");
            $table->foreign('id_autor')->references('id')->on('autor');
            $table->timestamps();
            $table->softDeletes();	
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
