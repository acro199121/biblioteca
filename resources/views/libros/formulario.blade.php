@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header"><a href="{{url("/libros")}}">Listado de libros</a> - Crear Libro </div>
    
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div class="col-lg-8 offset-lg-2 rounded shadow">
                        <form id="guardarLibro" method="post" >
                            
                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                        
                            <p><b>Los campos obligatorios se identificaran con un asterisco (*)</b></p>
                
                            <div class="form-group">
                                <label>nombre *</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Digitar el nombre del libro" required="required">
                            </div>

                            <div class="form-group">
                                <label>Fecha de publicación *</label>
                                <input type="date" class="form-control" name="fecha_publicacion" id="fecha_publicacion" required="required">
                            </div>

                            <div class="form-group">
                                <label>numero de paginas *</label>
                                <input type="number" class="form-control" id="num_paginas" name="num_paginas" placeholder="Digite el numero de paginas" required="required">
                            </div>

                            <div class="form-group">
                                <label>numero de copias *</label>
                                <input type="number" class="form-control" id="num_copias" name="num_copias" placeholder="Digite el numero de copias" required="required">
                            </div>

                            <div class="form-group">
                                <label>Autor *</label>
                                <select class="selectAut form-control" id="id_autor" name="id_autor" required="required">
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Breve reseña del libro</label>
                                <textarea class="form-control" rows="5" name="resena" id="resena" required="required">
                                </textarea>
                            </div>
                
                            <button type="submit" class="btn btn-primary mb-3">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/app/libro/libro.js')}}"></script>
@endsection