<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nombre
 * @property string $resena
 * @property string $tipo
 * @property int $estado
 * @property string $created_at
 * @property string $updated_at
 * @property ArticulosAutore[] $articulosAutores
 */
class articulos extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'resena', 'tipo', 'estado', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articulosAutores()
    {
        return $this->hasMany('App\ArticulosAutore', 'id_articulo');
    }
}
