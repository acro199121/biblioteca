jQuery(document).ready(function(){
                
    jQuery.ajax({
        url: '../../api/libros/selectLibros',
        method: 'POST',
        success: function(data) {
            let html = "<option value='0'>Seleccione el autor</option>";
            jQuery.each(data, function(index, value) {
                console.log("index - ",index);
                console.log("value - ",value);
                html += "<option value='"+value.id+"'>"+value.nombre+"</option>"
            });
            $(".selectAut").html(html);
        }
    });

   
    if($("#id").val()!=0) {
   
        jQuery.ajax({
            url: '../../api/libros/get/'+$("#id").val(),
            success: function(data) {

                jQuery.each(data.data, function(index, value) {
                    console.log("index - ",index);
                    console.log("value - ",value);
                    $("#"+index).val(value);
                });
            }
        });
  
    }
    
    jQuery('#guardarLibro').submit(function(event) {
        event.preventDefault();

        jQuery.ajax({
            url: '../../api/libros/store',
            type: 'POST',
            data: jQuery('#guardarLibro').serialize(),
            success: function(data) {
                alert(data.message);
                jQuery('#id').val(data.id);
            },
            error: function(data) {
                console.log(data.message);
            }
        });
    });
 });