<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function( Blueprint $table){
            $table->increments('id');
            $table->string('nombre',255);
            $table->engine = 'InnoDB';
        });

        \DB::table('profile')->insert(
            array(
                'nombre' => 'Administrador',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
