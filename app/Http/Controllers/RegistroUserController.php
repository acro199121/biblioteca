<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;
use Hash;

class RegistroUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $mensaje = '';
        $correo = '';
        $password = '';
        $status = '';

        try{
            $user = new users();
            $user->name = $request->post('name');
            $user->email = $request->post('email');
            $user->password = Hash::make($request->post('password'));
            $user->id_profile = $request->post('profile');
            $user->save();

            $query = users::find($user->id);

            if($query !== null){
                $mensaje = 'Usuario creado satisfactoriamente';
                $correo = $user->email;
                $password = $request->post('password');
                $status = 201;
            }
        
        }catch (\Illuminate\Database\QueryException $ex) {
            if($ex->getCode() == 23000){
                $mensaje = "No se pudo guardar el usuario. E-Mail Address duplicado";
            }else{
                $mensaje = 'No se pudo guardar el usuario. Por favor comunicarse con el administrador del sistema';
            }
            
            $status = 401;
        }

        $response = array(
            'status' => $status,
            'mensaje' => $mensaje,
            'correo' => $correo,
            'password' => $password
        );

        return response()->json($response);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(users $users)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, users $users)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(users $users)
    {
        //
    }
}
